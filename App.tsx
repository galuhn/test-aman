/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  TextInput,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icons from 'react-native-vector-icons/Feather';
import axios from 'axios'
import { BottomSheet, ListItem } from '@rneui/themed';

const App = () => {
  const [dataNegara, setDataNegara] = useState([]);
  const [dataPelabuhan,setDataPelabuhan] = useState([]);
  const [dataBarang,setDataBarang] = useState([]);

  const [modalBottom, setModalBottom] = useState(false);
  const [modalBottomPelabuhan,setModalBottomPelabuhan] = useState(false);
  const [modalBottomBarang,setModalBottomBarang] = useState(false);

  const [selectBarang,setSelectBarang] = useState('');
  const [selectBarangDesc,setSelectBarangDesc] = useState('');
  const [selectNegara,setSelectNegara] = useState('');
  const [selectPelabuhan,setSelectPelabuhan] = useState('');
  const [selectDisc,setSelectDisc] = useState('');
  const [selectHarga,setSelectHarga] = useState('');
  const [selectTotal,setSelectTotal] = useState('');

  useEffect(() => {
    getNegara()
  }, [])

  const getNegara = () => {
    axios.get("http://202.157.176.100:3000/negaras").then((response) => {
      console.log(response.data, 'response')
      setDataNegara(response.data)
    }).catch((error) => {
      throw (error)
    })
  }

  const getPelabuhan = (id: any) => {
    axios.get(`http://202.157.176.100:3000/pelabuhans?filter={"where" : {"id_negara":${id}}}`).then((response) => {
      console.log(response.data, 'response')
      setDataPelabuhan(response.data)
    }).catch((error) => {
      throw (error)
    })
  }

  const getBarang = (id: any) => {
    axios.get(`http://202.157.176.100:3000/barangs?filter={"where" : {"id_pelabuhan":${id}}}`).then((response) => {
      console.log(response.data, 'response barang')
      setDataBarang(response.data)
    }).catch((error) => {
      throw (error)
    })
  }

  return <>
    <SafeAreaView style={{ backgroundColor: "white", flex: 1 }}>
      <View style={{ marginHorizontal: 15, marginTop: 25 }}>
        <Text style={{ color: "black" }}>Negara</Text>
      </View>
      <View style={{ alignItems: "center" }}>
        <TouchableOpacity onPress={() => {
          setModalBottom(true)
        }} style={{ borderWidth: 1, width: Dimensions.get("screen").width * 0.90, marginTop: 15, height: 48, borderRadius: 10, borderColor: "gray" }}>
          <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 15, marginTop: 10 }}>
            <Text style={{ color: "black", fontSize: 14 }}>{selectNegara == "" ?  "Silakan Pilih Negara" : selectNegara}</Text>
            <Icons name='chevron-down' color={"black"} size={20} />
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ marginHorizontal: 15, marginTop: 15 }}>
        <Text style={{ color: "black" }}>Pelabuhan</Text>
      </View>
      <View style={{ alignItems: "center" }}>
        <TouchableOpacity onPress={() => {
          if(selectNegara == "") {
            Alert.alert("Peringatan","Silakan Pilih Negara Terlebih Dahulu!!")
          } else {
            setModalBottomPelabuhan(true)
          }
        }} style={{ borderWidth: 1, width: Dimensions.get("screen").width * 0.90, marginTop: 15, height: 48, borderRadius: 10, borderColor: "gray" }}>
          <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 15, marginTop: 10 }}>
            <Text style={{ color: "black", fontSize: 14 }}>{selectPelabuhan == "" ? "Silakan Pilih Pelabuhan" : selectPelabuhan}</Text>
            <Icons name='chevron-down' color={"black"} size={20} />
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ marginHorizontal: 15, marginTop: 15 }}>
        <Text style={{ color: "black" }}>Barang</Text>
      </View>
      <View style={{ alignItems: "center" }}>
        <TouchableOpacity onPress={() => {
          if(selectPelabuhan == "") {
            Alert.alert("Peringatan","Silakan Pilih Pelabuhan Terlebih Dahulu!!")
          } else {
            setModalBottomBarang(true)
          }
          
        }} style={{ borderWidth: 1, width: Dimensions.get("screen").width * 0.90, marginTop: 15, height: 48, borderRadius: 10, borderColor: "gray" }}>
          <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 15, marginTop: 10 }}>
            <Text style={{ color: "black", fontSize: 14 }}>{selectBarang == "" ? "Silakan Pilih Barang" : selectBarang}</Text>
            <Icons name='chevron-down' color={"black"} size={20} />
          </View>
        </TouchableOpacity>

        <View style={{
          borderWidth: 1,
          width: Dimensions.get("screen").width * 0.90,
          flexDirection: "row",
          borderRadius: 10,
          borderColor: "gray",
          marginTop:15
        }}>
          <TextInput
            style={{
              height: 150,
              color: "black",
              flex: 1,
              marginLeft: 10
            }}
            multiline={true}
            onChangeText={(text) => setSelectBarangDesc(text)}
            value={selectBarangDesc}
            editable={false}
            textAlignVertical="top"
            placeholder="Masukan barang anda..."
            placeholderTextColor="gray"
          />
        </View>
      </View>

      <View style={{ marginHorizontal: 15, marginTop: 15 }}>
        <Text style={{ color: "black" }}>Diskon</Text>
      </View>
      <View style={{marginLeft:15,flexDirection: "row"}}>
        <View style={{
          borderWidth: 1,
          width: Dimensions.get("screen").width * 0.18,
          flexDirection: "row",
          borderRadius: 10,
          borderColor: "gray",
          marginTop: 15
        }}>

          <TextInput
            style={{
              height: 40,
              color: "black",
              flex: 1,
              width: '100%',
              marginLeft: 10
            }}
            multiline={true}
            onChangeText={(text) => setSelectDisc(text)}
            value={selectDisc}
            editable={false}
            textAlignVertical="top"
            placeholder="Diskon"
            placeholderTextColor="gray"
          />
        </View>
        <Text style={{color: "black",marginTop:25,marginLeft:10}}>%</Text>
      </View>

      <View style={{ marginHorizontal: 15, marginTop: 15 }}>
        <Text style={{ color: "black" }}>Harga</Text>
      </View>
      <View style={{marginLeft:15}}>
        <View style={{
          borderWidth: 1,
          width: Dimensions.get("screen").width * 0.40,
          flexDirection: "row",
          borderRadius: 10,
          borderColor: "gray",
          marginTop: 15
        }}>

          <TextInput
            style={{
              height: 40,
              color: "black",
              flex: 1,
              width: '100%',
              marginLeft: 10
            }}
            multiline={true}
            onChangeText={(text) => setSelectHarga(text)}
            value={`Rp.${selectHarga}`}
            editable={false}
            textAlignVertical="top"
            placeholder="Harga"
            placeholderTextColor="gray"
          />
        </View>
      </View>

      <View style={{ marginHorizontal: 15, marginTop: 15 }}>
        <Text style={{ color: "black" }}>Total</Text>
      </View>
      <View style={{marginLeft:15}}>
        <View style={{
          borderWidth: 1,
          width: Dimensions.get("screen").width * 0.40,
          flexDirection: "row",
          borderRadius: 10,
          borderColor: "gray",
          marginTop: 15
        }}>

          <TextInput
            style={{
              height: 40,
              color: "black",
              flex: 1,
              width: '100%',
              marginLeft: 10
            }}
            multiline={true}
            onChangeText={(text) => setSelectTotal(text)}
            value={`Rp.${selectTotal}`}
            editable={false}
            textAlignVertical="top"
            placeholder="Total"
            placeholderTextColor="gray"
          />
        </View>
      </View>
    </SafeAreaView>
    <BottomSheet modalProps={{}} isVisible={modalBottom}>
      <View style={{ backgroundColor: "white", height: 380, borderTopLeftRadius: 25, borderTopRightRadius: 25 }}>
        <View style={{ paddingHorizontal: 15, marginTop: 20, flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={{ color: "black", fontSize: 14, fontWeight: "bold" }}>List Negara</Text>
          <TouchableOpacity onPress={() => {
            setModalBottom(false)
          }}>
            <Icons name="x" size={20} color={"black"} />
          </TouchableOpacity>
        </View>
        <View style={{marginTop:15}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {dataNegara.map((response, index) => (
              <TouchableOpacity onPress={() => {
                getPelabuhan(response.id_negara)
                setSelectNegara(`${response.kode_negara} - ${response.nama_negara}`)
                setModalBottom(false)
                setSelectPelabuhan('')
                setSelectBarang('')
                setSelectBarangDesc('')
                setSelectDisc('')
                setSelectHarga('')
                setSelectTotal('')
              }} style={{ paddingHorizontal: 15 }}>
                <Text style={{ color: "black",marginTop:10 }}>{response.kode_negara} - {response?.nama_negara}</Text>
                {index < dataNegara.length - 1 ? <View style={{borderBottomWidth: 1,borderBottomColor: "black",marginTop: 15}}/> : null}
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
    </BottomSheet>

    <BottomSheet modalProps={{}} isVisible={modalBottomPelabuhan}>
      <View style={{ backgroundColor: "white", height: 380, borderTopLeftRadius: 25, borderTopRightRadius: 25 }}>
        <View style={{ paddingHorizontal: 15, marginTop: 20, flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={{ color: "black", fontSize: 14, fontWeight: "bold" }}>List Pelabuhan</Text>
          <TouchableOpacity onPress={() => {
            setModalBottomPelabuhan(false)
          }}>
            <Icons name="x" size={20} color={"black"} />
          </TouchableOpacity>
        </View>
        <View style={{marginTop:15}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {dataPelabuhan.map((response, index) => (
              <TouchableOpacity onPress={() => {
                // getPelabuhan(response.id_negara)
                setSelectPelabuhan(response.nama_pelabuhan)
                getBarang(response.id_pelabuhan)
                setModalBottomPelabuhan(false)
                setSelectBarang('')
                setSelectBarangDesc('')
                setSelectDisc('')
                setSelectHarga('')
                setSelectTotal('')
              }} style={{ paddingHorizontal: 15 }}>
                <Text style={{ color: "black",marginTop:10 }}>{response?.nama_pelabuhan}</Text>
                {index < dataPelabuhan.length - 1 ? <View style={{borderBottomWidth: 1,borderBottomColor: "black",marginTop: 15}}/> : null}
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
    </BottomSheet>

    <BottomSheet modalProps={{}} isVisible={modalBottomBarang}>
      <View style={{ backgroundColor: "white", height: 380, borderTopLeftRadius: 25, borderTopRightRadius: 25 }}>
        <View style={{ paddingHorizontal: 15, marginTop: 20, flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={{ color: "black", fontSize: 14, fontWeight: "bold" }}>List Barang</Text>
          <TouchableOpacity onPress={() => {
            setModalBottomBarang(false)
          }}>
            <Icons name="x" size={20} color={"black"} />
          </TouchableOpacity>
        </View>
        <View style={{marginTop:15}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {dataBarang.map((response, index) => (
              <TouchableOpacity onPress={() => {
                let harga = response.harga
                let diskonPersen = response.diskon
                let diskon = (diskonPersen / 100 ) * harga

                let resultTotal = harga - diskon
                setSelectBarang(response.nama_barang)
                setModalBottomBarang(false)
                setSelectBarangDesc(response.description)
                setSelectDisc(response.diskon.toString())
                setSelectHarga(response.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                setSelectTotal(resultTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
              }} style={{ paddingHorizontal: 15 }}>
                <Text style={{ color: "black",marginTop:10 }}>{response?.nama_barang}</Text>
                {index < dataBarang.length - 1 ? <View style={{borderBottomWidth: 1,borderBottomColor: "black",marginTop: 15}}/> : null}
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
    </BottomSheet>
  </>
}

export default App;
